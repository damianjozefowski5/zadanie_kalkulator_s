package com.awebo.app;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.awebo.logic.calculators.SalaryCalculator;
import com.awebo.logic.calculators.SalaryCalculatorFactory;
import com.awebo.logic.calculators.SalaryCalculatorFactoryImpl;
import com.awebo.logic.converters.CurrencyConverter;
import com.awebo.logic.converters.CurrencyConverterFactory;
import com.awebo.logic.converters.CurrencyConverterFactoryImpl;
import com.awebo.models.Salary;

@RestController
public class SalariesController {
	
	@RequestMapping("/salaries/count")
	public Salary getResponse(@RequestParam(value="salary", defaultValue="0") float daySalary,
			@RequestParam(value="country") String country,
			@RequestParam(value="resultCurrency") String resultCurrency) throws Exception {
		
		Salary salary = new Salary();
		SalaryCalculatorFactory calculatorsFactory = new SalaryCalculatorFactoryImpl();
		SalaryCalculator salaryCalc = calculatorsFactory.getSalaryCalculator(country);
		
		CurrencyConverterFactory currencyFactory = new CurrencyConverterFactoryImpl();
		CurrencyConverter currencyConverter = currencyFactory.getCurrencyConverter("PLN");
		float rate = currencyConverter.getRate(salaryCalc.currency());
		
		salary.setSalary(salaryCalc.countSalary(daySalary) * rate);
		salary.setCurrency(resultCurrency);
		
		return salary;
	}
	
}
