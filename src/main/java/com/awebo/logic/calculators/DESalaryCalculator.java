package com.awebo.logic.calculators;

public class DESalaryCalculator extends SalaryCalculator {
	
	private static float TAX = 20.0f;
	private static float FIXED_COSTS  = 800.0f;
	private static final String CURRENCY = "EUR";

	public float taxValue() {
		return TAX;
	}

	public float fixedCosts() {
		return FIXED_COSTS;
	}

	public String currency() {
		return CURRENCY;
	}
}
