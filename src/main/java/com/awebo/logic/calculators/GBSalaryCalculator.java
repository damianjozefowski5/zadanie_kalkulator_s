package com.awebo.logic.calculators;

public class GBSalaryCalculator extends SalaryCalculator {
	
	private static float TAX = 25.0f;
	private static float FIXED_COSTS  = 600.0f;
	private static final String CURRENCY = "GBP";

	public float taxValue() {
		return TAX;
	}

	public float fixedCosts() {
		return FIXED_COSTS;
	}
	
	public String currency() {
		return CURRENCY;
	}
}
