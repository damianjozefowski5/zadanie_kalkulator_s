package com.awebo.logic.calculators;

public class PLSalaryCalculator extends SalaryCalculator {
	
	private static float TAX = 19.0f;
	private static float FIXED_COSTS  = 1200.0f;
	private static final String CURRENCY = "PLN";

	public float taxValue() {
		return TAX;
	}
	
	public float fixedCosts() {
		return FIXED_COSTS;
	}
	
	public String currency() {
		return CURRENCY;
	}
}
