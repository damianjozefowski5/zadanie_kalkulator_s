package com.awebo.logic.calculators;

public abstract class SalaryCalculator {
	
	private static Integer WORKING_DAYS = 22;
	
	public abstract float taxValue();
	public abstract float fixedCosts();
	public abstract String currency();
	
	public float countSalary(float salary) {
		return ((salary*WORKING_DAYS)*((100 - taxValue())/100))-fixedCosts();
	};
}
