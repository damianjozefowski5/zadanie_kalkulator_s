package com.awebo.logic.calculators;

public interface SalaryCalculatorFactory {
	
	SalaryCalculator getSalaryCalculator(String country);
}
