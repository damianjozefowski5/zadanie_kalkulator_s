package com.awebo.logic.calculators;


public class SalaryCalculatorFactoryImpl implements SalaryCalculatorFactory {

	public SalaryCalculator getSalaryCalculator(String country) {
		SalaryCalculator salaryCalc;
		switch(country.toLowerCase()) {
			case "pl":
				salaryCalc =  new PLSalaryCalculator();
				break;
			case "gb":
				salaryCalc =  new GBSalaryCalculator();
				break;
			case "de":
				salaryCalc = new DESalaryCalculator();
				break;
			default:
				salaryCalc = null;
		}
		
		if(salaryCalc == null) {
			throw new UnsupportedOperationException();
		}
		return salaryCalc;
	}
}
