package com.awebo.logic.converters;

public interface CurrencyConverter {
	
	float getRate(String targetCurrency) throws Exception;
	
}
