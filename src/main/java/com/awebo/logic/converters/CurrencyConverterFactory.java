package com.awebo.logic.converters;

public interface CurrencyConverterFactory {

	CurrencyConverter getCurrencyConverter(String sourceCurrency);
}
