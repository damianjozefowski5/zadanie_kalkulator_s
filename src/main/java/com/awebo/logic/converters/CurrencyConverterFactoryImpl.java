package com.awebo.logic.converters;

public class CurrencyConverterFactoryImpl implements CurrencyConverterFactory {

	@Override
	public CurrencyConverter getCurrencyConverter(String sourceCurrency) {
		CurrencyConverter converter;
		switch(sourceCurrency) {
			case "PLN":
				converter = new PLNCurrencyConverter();
				break;
			default :
				converter = null;
		}
		
		if(converter == null) {
			throw new UnsupportedOperationException();
		}
		
		return converter;
	}

	

}
