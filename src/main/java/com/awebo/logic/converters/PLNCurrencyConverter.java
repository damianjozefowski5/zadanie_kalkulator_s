package com.awebo.logic.converters;

import java.io.IOException;
import java.net.URI;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.SimpleClientHttpRequestFactory;

public class PLNCurrencyConverter implements CurrencyConverter{

	private static final String API_ENDPOINT = "http://api.nbp.pl/api/exchangerates/rates/A/";
	private static final String CURRENCY = "PLN";
	
	public float getRate(String targetCurrency) throws Exception {
		
		if(CURRENCY.equals(targetCurrency)) {
			return 1;
		}
		
	    SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
	    URI uri = new URI(API_ENDPOINT + "/" + targetCurrency);
	    HttpMethod method = HttpMethod.GET;
	    ClientHttpRequest request = factory.createRequest(uri, method);
	    ClientHttpResponse response = request.execute();
	    
	    if(response.getRawStatusCode() != 200) {
	    	throw new Exception("Could not get rate for currency " + targetCurrency);
	    }
	    
	    float rate = parseApiResponse(response);
	    
	    return rate;
	}
	
	/*
	 * API response {"table":"A","currency":"euro","code":"EUR","rates":[{"no":"232/A/NBP/2017","effectiveDate":"2017-11-30","mid":4.2055}]}
	 * */
	private float parseApiResponse(ClientHttpResponse response) throws JSONException, IOException {
		JSONObject obj = new JSONObject(IOUtils.toString(response.getBody(), "UTF-8"));
		JSONArray arr = obj.getJSONArray("rates");
		JSONObject rateObject = arr.getJSONObject(0);
		float rate = (float)rateObject.getDouble("mid");
		return rate;
	}
	

}
