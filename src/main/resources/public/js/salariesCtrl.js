angular.module('salariesCalcApp')

.controller('MainCtrl', function MainCtrl(salariesDataService, $http) {
	var self = this;
	self.data = {
		givenSalary: 0,
		resultSalary: 0,
		resultCurrency: 'PLN',
		country: 'PL',
		countries: [{name: "Polska", code: "PL"}, 
					{name: "Niemcy", code: "DE"},
					{name: "Wielka Brytania", code: "GB"}]
	};
	
	this.countSalary = function() {
		if(!self.data.givenSalary || self.data.givenSalary <= 0) {
			return;
		}
		salariesDataService.getSalary(self.data).then
		(function(res) {
			self.data.resultSalary = parseFloat(res.data.salary).toFixed(2);
		}, function(err) {
			console.log(err);
		})
	};
});