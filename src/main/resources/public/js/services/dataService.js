angular.module('salariesCalcApp')

.service('salariesDataService', function($http, api) {	
	this.getSalary = function(data) {
		var url = api.getHost() + "/salaries/count";
		var promise = $http({
			method: "GET",
			url: url,
			params: {
				salary: data.givenSalary,
				country: data.country,						
				resultCurrency: data.resultCurrency
			}
		});
		return promise;
	};
});